# شروع کار با shell

## دانستنی‌ها

- لینوکس برخلاف ویندوز به کوچک-بزرگی حروف حساس است؛ یعنی در لینوکس دستورهای `Ls` و `ls` متفاوت است. و همچنین می‌توان پوشه‌های `Folder` و `folder` هم متفاوت است.
- در لینوکس، بعضی از دسترسی‌ها تنها به کاربر `root` داده شده. از دستور `sudo su -` برای ورود به‌عنوان کاربر `root` می‌توان استفید.

## خاموشیدن و روشنیدن

```shell
# shutdown [OPTIONS...] [TIME] [WALL...]
sudo shutdown -h -r +10 "System will turn off in 10 min"
```

## گرفتن مشخصات پردازنده

```shell
uname -a
uname -i
getconf LONG_BIT
```

## گرفتن مسیر جاری

```shell
pwd # print working directory
```

## گرفتن فهرست پوشه/فایل‌ها

```shell
ls -alh . # List directory contents, a -> all file, l -> list format, h -> human readable form
ll . # == ls -al
```

## تغیریدن مسیر

```shell
cd ~/
```

## گرفتن راهنمای دستور

```shell
ls --help
# OR
man man
man 7 man
```

## جستجوی دستور

```shell
man -k pw
# OR
apropos pw
```

## ایجادیدن یه فایل خالی

```shell
touch ~/Documents/test_file.txt
```

## حذفیدن یه فایل

```shell
rm -rf ~/Documents/test_file.txt
```

## خواندن محتوای یه فایل

```shell
cat ~/Documents/test_file.txt
# چند خط اول
head -20 ~/Documents/test_file.txt
# OR
tail -20 ~/Documents/test_file.txt
```

### پسندوریدن خط‌های شامل یه عبارت

```shell
sudo grep opend /var/log/auth.log
```

### صفحه‌بندی خروجی یه دستور

```shell
sudo grep opend /var/log/auth.log | more
# OR
sudo grep opend /var/log/auth.log | less
```

## جستجوی فایل

```shell
find ~/ --name test_file.txt
```
