# BASH

## یافتن متن در فایل

### grep

برآمده از Global Regular Expression Print، برای یافتن یه متن در یک یا چند قایل:

```shell
# Get lines that have <a_word> in them
# OPTIONS:
# -v: Invert the sense of matching, to select non-matching lines.
# -i: Ignore case distinctions in patterns and input data, so that characters that differ only in case match each other.
grep <regex> <file_path>

# Count all file lines
wc -l
```

### zgrep

برای جستجوییدن در فایل‌های فشرده شده:

```shell
zgrep <regex> <file_path>
```

## کنترل جریان داده

در لینوکس سه جریان داده وجود دارد که به‌شکل استاندارد به درگاه‌هایی هدایت می‌شود:

- ورودی: صفحه کلید
- خروجی: صفحه نمایش
- خطا: صفحه نمایش

ولی می‌توان یا عملگرهایی جریان را هدایت کرد.

### Piping

افزودن برنامه میانی در جریان داده خروجی:

```shell
ll -h | grep <regex> | grep <regex>
```

### بازراهیدن جریان خروجی

راهیدن جریان داده خروجی به درون یه فایل:

```shell
# Write to a file
<command> > <path_to_file>
# Append to a file
<command> >> <path_to_file>
```

### بازراهیدن جریان خطا

راهیدن جریان داده خطا به درگاه دیگر:

```shell
ll -h 2> dev null
ll -h 2>> filename
```

### بازراهیدن جریان ورودی

راهیدن داده‌های یه فایل به جریان داده ورودی:

```shell
<command> < filename
```

### tee

برگرفتن از جریان داده خروجی:

```shell
ll -h | tee <file_1> <file_2> ...
```

### ترجمه جریان خروجی به آرگومان

برخی از دستورها گیرنده‌ی جریان ورودی استاندارد نیستند. در این موردها می‌توان با از دستور `xargs` جریان داده‌ی خروجی را به آرگومان ترجمه کرد:

```shell
ll -p | xargs echo
```

## BASH Scripting

برای اجرای مجموعه‌ای از دستورها توسط `bash` آن‌ها را در یه فایل اجرایی `*.sh` با ساختار زیر می‌نویسیم:

```sh
#!/bin/bash

# Comments
```

و به شکل زیر می‌اجراییم:

```bash
./<file_name>.sh
# OR
bash <file_name>.sh
```
