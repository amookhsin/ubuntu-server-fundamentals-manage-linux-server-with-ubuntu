# پیکربندی دسترسی از دور

## آمادن سیستم‌عالم

1. نصب `openssh-server`

   <div dir="ltr" align="left" markdown="1">

   ```shell
   sudo apt update
   sudo apt upgrade
   sudo apt full-upgrade
   sudo apt install openssh-server
   ```

   </div>

2. پیکریدن دیوارآتش

   <div dir="ltr" align="left" markdown="1">

   ```shell
   sudo ufw allow ssh
   sudo ufw default deny incoming
   sudo ufw default allow outgoing
   sudo ufw enable
   sudo ufw status verbose
   ```

   </div>

3. اجراییدن سرور ssh

   <div dir="ltr" align="left" markdown="1">

   ```shell
   sudo service ssh start
   sudo service ssh status
   sudo netstat -tanup | grep ssh
   ```

   </div>

## وصلیدن با ssh توسط نام‌کاربری

<div dir="ltr" align="left" markdown="1">

```shell
ssh-keygen
ssh-copy-id <username>@<host_id>
ssh <username>@<host_id>
```

</div>

## مدیریت ssh

پس از احرازیدن با نام کاربری:

<div dir="ltr" align="left" markdown="1">

```shell
sudo vim /etc/ssh/sshd_config.d/10-my-sshd-stuff.conf
```

</div>

درجیدن:

<div dir="ltr" align="left" markdown="1">

```conf
# https://man.openbsd.org/ssh_config
# My sshd settings
DebianBanner no
DisableForwarding yes
PermitRootLogin no
PasswordAuthentication no
PermitEmptyPasswords no
```

</div>

بازبارگذاری پیکربندی:

<div dir="ltr" align="left" markdown="1">

```shell
sudo service ssh reload
```

</div>
