# مدیرش کاربرها

## دانستنی

اطلاعات کاربرها در فایل `/etc/passwd` و گذرواژه کاربرها در فایل `/etc/shadow` نگهداری می‌شوند که با دستورهای زیر می‌توان به‌شکل مستقیم ویرایش‌اشان کردن:

```shell
vipw
vipw -s
```

## افزودن کاربر

```shell
sudo useradd -d /home/mohammad -m mohammad
sudo passwd mohammad

# OR

sudo adduser ali
```

## افزودن یه کاربر به گروه `sudo`

```shell
sudo usermod -aG sudo <username>
cat /etc/group | grep <username>
```

## ورود با یه کاربر

```shell
su <username>
```

## قفلیدن و ناقفلیدن یه کاربر

```shell
# Lock
sudo usermod -L <username>

# Unlock
sudo usermod -U <username>
```

## حذفیدن یه کاربر

```shell
deluser --remove-home --remove-all-files --backup --backup-to <DIR> <username>
```
