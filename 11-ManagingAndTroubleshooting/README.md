# مدیریت و عیبیابی

## دیدن وضعیت فرآیندهای در اجرا

دیدن منابعی که هر فرآیند می‌مصرفد.

### دستور `ps`

دیدن یه برگرفت از وضعیت سیستم.

```shell
# To see current user's process
ps

# To see all the running processes
ps aux

# To sort by memory usage
ps aux --sort=-pcpu,+pmem   # cpu: lowest to highest, mem: highest to lowest
```

مفهوم نمادها:

<div dir="rtl" align="right" markdown="1">

- PID: شناسه فرآیند؛
- <span dir="ltr" markdown="1">%CPU</span>: میزان پردازنده‌ی به‌کارگرفته؛
- <span dir="ltr" markdown="1">%mem</span>: میزان رم به اشغالیده به درصد؛
- VSZ: حافظه‌ی مجازی استفیده شده در کل فرآیند؛
- RSS TTY: میزان رم اشغالیده برحسب کیلو بایت؛
- STAT: وضعیت فرآیند چند-حرفی؛

  - حرف نخست:
    - R: اجراییده یا اجراپذیر؛
    - D: در انتظار یه ورودی/خروجی (بی‌وقفه‌پذیر، پردازنده تا دریافت نتیجه مشغول‌اش است)
    - S: در انتظار یه ورودی/خروجی (وقفه‌پذیر، پردازنده می‌توان تا رسیدن نتیجه به کاری دیگر بپردازد)
    - Z: بسته‌شده ولی از حافظه به‌درستی خارج نشده.
    - T: متوقفیده با سیگنال SIGSTOP و منتظر سیگنال بسته‌شدن یا ادامیدن است.
  - حرف دوم:
    - <: اولویت بالا؛
    - N: اولویت پایین؛
    - L: has pages locked into memory
    - s: پشرو نشست؛
    - l: چند-ریسمانی؛

</div>

### دستور `top`

دیدن زنده‌ی وضعیت سیستم.

```shell
# To run it with default options
top
```

## دیدن وضعیت دیسک-سخت

```shell
# To see the status of hard drive
df

# if you want to see file system types
df -T

# To see sizes as human readable
df -h

# To show just a certain file system
sf -t <file_system_type>
```

## بررسیدن وضعیت حافظه

```shell
# To checking out memory utilization
free

# To show sizes as human readable
free -h

# To see the snapshot over time -> -c: the number on time, -s: to specify how frequently it running
free -c 5 -s 1

# To call free every 2 seconds
watch free
# To highlight difference
watch -d free

# To define the refresh time
watch -n 1 free
```

## برنامریزیدن برای اجرای دستور

برای اجراییدن دستور/دستورهایی براساس یه زمانبندی خاص از ابزار `crontab` می‌استفیم.

```shell
crontab -e
```

که بنابر راهنمای خودش، باید به‌شکل زیر دوره‌های اجرای هر دستور را درج کنیم. به‌عنوان مثال، در زیر `<path_to_a_script>` در هر دقیقه اجرا می‌شود.

```conf
# min | hour | day of month | month | day of week | command
  *       *         *           *          *        <path_to_a_script>
```

**توجه.** برای تعیین هر پارامتر زمانی تنها کافیه یک یا چند فاصله بین‌اشان نهاد.

اجرای `<path_to_a_script>` در هر ۱۵ دقیقه:

```conf
# m h  dom mon dow   command
  15,30,45 * * * * <path_to_a_script>
# OR
 */15 * * * * <path_to_a_script>
```

**[یه ویرایشگر سریع برای `crontab`](https://crontab.guru/)**
