# امنیدن سرور

## محدودیدن تلاش‌های ناموفق

با ابزار منبع‌باز `fail2ban` می‌توان تعداد تلاش‌های ناموفق از هر آی‌پی را محدود کرد.

```shell
# Updating packages
sudo apt update && sudo apt upgrade

# Installing fail2ban
sudo apt install fail2ban sendmail iptables-persistent

# Configuration fail2ban
sudo vim /etc/fail2ban/jail.local

# To enable fail2ban
sudo service fail2ban start

# To check the status of the service
sudo fail2ban-client status sshd
```

محتوای `jail.local`:

```conf
[DEFAULT]
# Ban hosts for one hour:
bantime = 3600

# Override /etc/fail2ban/jail.d/00-firewalld.conf:
banaction = iptables-multiport

[sshd]
enabled = true
```

## پیکریدن دیوار آتش

با استفاده از ابزار `ufw` می‌توان دیوارآتش را به‌سادگی پیکرید. این ابزار پیشفرض بر اوبونتو نصب است.

```shell
# To check the ufw status
sudo ufw status
# To look at the ip addresses
sudo ip addr

# To change the default firewall configuration
sudo vim /etc/default/ufw
```

یه پیکربندی استاندار ابتدایی پذیرفتن همه‌ی ترافیک‌های خروجی و مسدودیدن همه‌ی ترافیک‌ها ورودی به‌شکل پیشفرض است.

```shell
sudo ufw default deny incoming
sudo ufw default allow outgoing

# To allow the ssh to remote the server
sudo ufw allow ssh
# To allow http and https protocol
sudo ufw allow http
sudo ufw allow https

# To enable ufw
sudo ufw enable
# To look at the status
sudo ufw status
sudo ufw status verbose

# To look at the ip table rules
sudo iptables -L
```

### حذفیدن یه قانون از دیوارآتش

```shell
# To see the number of each rule
sudo ufw status numbers

# To delete a rule
sudo ufw delete <rule_num>
```

## نافعالیدن سرویس‌های زاید

برای نافعالیدن هر سرویس باید دو گام نافعالیدن سرویس‌اِ و مسدودیدن‌اش توسط دیوارآتش را انجامید. برای مثال سرویس IPv6 در زیر نافعالیده شده.

**هشدار.** تنها هنگامی سرویس IPv6 نافعالیده شود که سرورمان تنها ترافیک‌های IPv4 را می‌دریابد.

```shell
# To see what services are listing on the server
netstat -tanup # TCP All Numeric UDP Program
```

### نافعالیدن IPv6

ساختن یه فایل پیکرپندی با نام دلخواه در مسیر `/etc/sysctl.d/99-my-disable-ipv6.conf` با محتوای زیر:

```conf
net.ipv6.conf.all.disable_ipv6 = 1
net.ipv6.conf.default.disable_ipv6 = 1
net.ipv6.conf.lo.disable_ipv6 = 1
```

و سپس بازآغازاندن `procps`:

```shell
sudo service procps restart

# To make sure that IPv6 is disable
cat /proc/sys/net/ipv6/conf/all/disable_ipv6
```

سپس، برای نافعالیدن IPv6 در ssh، فایل `/etc/ssh/sshd_config` را همانند زیر می‌ویراییم:

```conf
# ...

#Port 22
#AddressFamily any
ListenAddress 0.0.0.0
#ListenAddress ::

# ...
```

سپس برای اعمال تغییرها سرویس ssh را می‌بازآغازانیم.

```shell
sudo service ssh restart

# To see what services are listing on the server
netstat -tanup
```

#### نافعالیدن IPv6 در دیوارآتش

فایل `/etc/default/ufw` را همانند زیر می‌ویراییم:

```conf
# Set to yes to apply rules to support IPv6 (no means only IPv6 on loopback
# accepted). You will need to 'disable' and then 'enable' the firewall for
# the changes to take affect.
IPV6=no
```

سپس برای اعمال تغییرها سرویس ufw را یک بار می‌بازآغازانیم:

```shell
sudo service ufw restart

# To check ufw status
sudo ufw status
```

## مدیریت مجوزهای sudo (Managing sudo Permissions)

با دستور `visudo` می‌توان اقدام‌هایی که هر کاربر می‌تواند با sudo کند را محدود کنیم. برای این کافیه فایل قانون‌ها را در مسیر `/etc/sudoers.d/` قرار دهیم:

```shell
# To add a rule file (if the file didn't exsict it will be created)
sudo visudo -f /etc/sudoers.d/my-sudoers
```

**نمونه‌ای از کلیت یه فایل قانون:**

```conf
# My sudoers file

# Host alias specification

# User alias specification

#User_Alias WEBMASTERS=luke, leia

# Cmnd alias specification

#Cmnd_Alias WEB=/etc/init.d/nginx status, /etc/init.d/nginx reload, /etc/init.d/nginx start, /etc/init.d/nginx restart, /etc/init.d/nginx stop

# Runas alias specification

# User Privilege Lines

#luke ALL=/etc/init.d/nginx status, /etc/init.d/nginx reload, /etc/init.d/nginx start, /etc/init.d/nginx restart, /etc/init.d/nginx stop

#WEBMASTERS     ALL=WEB

# Group Privilege Lines
```

**منبع.** برای کسب اطلاع‌های بیشتر می‌توان از دستور زیر استفید:

```shell
man sudo
```
