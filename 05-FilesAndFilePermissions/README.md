# فایل‌ها و مجوزها

## ایجادیدن پوشه

```shell
mkdir <dir_name>
```

## تغییر مجوز دسترسی یه فایل

```shell
chmod u=wrx <file_name>
chmod g-wrx <file_name>
chmod o+wrx <file_name>
```

## پنهانیدن فایل

نام هر فایل/پوشه‌ای که با `.` شروع شود، پنهان است.

```shell
mkdir .HiddenDir
```

## کپیدن فایل

```shell
# Prompt before overwrite (overrides a previous -n option)
cp -i <path_to_origin> <path_to_dist>
# Copy directories recursively
cp -r <path_to_origin> <path_to_dist>
```

## حذفیدن فایل/پوشه

```shell
rm <path_to_files>
# remove directories and their contents recursively
rm -rf <path_to_files>
```

## امحاییدن فایل

حذفیدن امن یه فایل:

```shell
shred -v <path_to_file>
# Deallocate and remove file after overwriting
shred -vu <path_to_file>
```

**توجه:** از `wipe` می‌توان برای امحای پوشه استفید.

## انتقالیدن فایل/پوشه

```shell
mv <path_to_origin> <path_to_dist>
```

**نکته:** از این دستور می‌توان برای تغییرنام نیز استفاده کرد:

```shell
mv file_name.txt new_name.txt
```

## پیوندیدن فایل

**توجه:** هر فایل توسط `inode` که یه شماره‌ی یکتا برای گره‌ی فایل است، توسط سیستم‌عامل شناخته می‌شود.

```shell
# list inode information instead of block usage
df -i
# print the index number of each file
ls -li
```

### Soft Link

تنها یه میانبر به فایل اصلی است:

```shell
ln -s <path_to_origin> <path_to_dist>
```

### Hard Link

همانند تعریف یه اشارگر جدید به یه محتوا در سطح دیسک است:

```shell
ln <path_to_origin> <path_to_dist>
```
