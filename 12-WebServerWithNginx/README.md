# ساختن سرور وب با Nginx

1. نصبیدن

   <div dir="ltr" align="left" markdown="1">

   ```shell
   # To update DB repository and OS
   sudo apt update && sudo apt -y upgrade && sudo apt -y full-upgrade

   # To install nginx
   sudo apt install nginx

   # To start nginx
   sudo systemctl start nginx
   # To restart nginx
   sudo systemctl restart nginx
   # To reload nginx
   sudo systemctl reload nginx
   # To stop nginx
   sudo systemctl stop nginx

   # To check nginx status
   sudo systemctl status nginx
   #
   ```

   </div>

2. فعالیدن http و https در دیوارآتش

   <div dir="ltr" align="left" markdown="1">

   ```shell
   sudo ufw allow http
   sudo ufw allow https

   # To check firewall status
   sudo ufw status
   ```

   </div>

## امنیدن سرور

به‌دلیل نیاز به سرور در اینترنت تماشای این بخش را به آینده مکولیدم.
