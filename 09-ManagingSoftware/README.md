# مدیریت بسته‌های نرم‌افزاری

هنگام اجرای دستور `apt update` اوبونتو مخزن‌هایی (repository) مشخص‌شده در زیر مشخص‌شده اند بررسی می‌کند:

```sehll
grep /etc/apt/sources.list
ll /etc/apt/sources.list.d
```

## بروزیدن بسته‌های نرم‌افزاری

```shell
# Update the database
sudo apt update
# Get list off upgradable apps
apt list --upgradable
# Upgrade all available softwares uptodate
sudo apt upgrade
# To remove all packages if required to install another packages
sudo apt full-upgrade
# To remove all packages that no longer needed
sudo apt autoremove
```

## ارتقایدن سیستم‌عامل

```shell
# To see version of Ubuntu
lsb_release -a
# To upgrade to a newer release
sudo do-release-upgrade
```

## جستجوی نرم‌افزار

```shell
# Searching an app
apt search <app_name>
# Look at the app detail
apt show <app_name>
# Look at app dependencies
apt depends <app_name>
```

## نصبیدن/حذفیدن نرم‌افزار

```shell
# To make sure to install the latest version
sudo apt update
# To install an app
sudo apt install <package_name>
# To remove an app
sudo apt remove <package_name>
# To remove an app with all related configurations
sudo apt purge
```
