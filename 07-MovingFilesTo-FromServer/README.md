# تبادل فایل/پوشه

## سرور

```shell
# Copy file to server
scp <path_to_target> <username>:<ip_address>:<path_to_dist>

# Copy file from server
scp <username>:<ip_address>:<path_to_target> <path_to_dist>

# Copy folder to-from server
scp -r <path_to_target> <username>:<ip_address>:<path_to_dist>
```

## دریافت از تارنما

```shell
# WGET
wget <url_to_file>
# Get all pages of a site
wget -r <target_site_url>

# CRUL
crul -O <urt_to_file>
```
